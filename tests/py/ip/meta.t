:input;type filter hook input priority 0

*ip;test-ip4;input

icmp type echo-request;ok
meta time "1970-05-23 21:07:14" drop;ok
meta time 12341234 drop;ok;meta time "1970-05-23 21:07:14" drop
meta time "2019-06-21 17:00:00" drop;ok
meta time "2019-07-01 00:00:00" drop;ok
meta time "2019-07-01 00:01:00" drop;ok
meta time "2019-07-01 00:00:01" drop;ok
meta day "Sat" drop;ok
meta day "Saturday" drop;ok;meta day "Sat" drop
meta day 6 drop;ok;meta day "Sat" drop
meta day "Sa" drop;fail
meta hour "17:00" drop;ok
meta hour "17:00:00" drop;ok;meta hour "17:00" drop
meta hour "17:00:01" drop;ok
meta hour "00:00" drop;ok
meta hour "00:01" drop;ok
meta l4proto icmp icmp type echo-request;ok;icmp type echo-request
meta l4proto ipv6-icmp icmpv6 type nd-router-advert;ok;icmpv6 type nd-router-advert
meta l4proto 58 icmpv6 type nd-router-advert;ok;icmpv6 type nd-router-advert
icmpv6 type nd-router-advert;ok

meta ibrname "br0";fail
meta obrname "br0";fail
